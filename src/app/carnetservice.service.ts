import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CarnetserviceService {

  public url:string="http://localhost:9091/contrats"
  constructor(private httpClient: HttpClient) { }

  public deleteContrat(url){
    return this.httpClient.delete(url);
  }
}
