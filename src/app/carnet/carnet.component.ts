import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CarnetserviceService } from '../carnetservice.service';

@Component({
  selector: 'app-carnet',
  templateUrl: './carnet.component.html',
  styleUrls: ['./carnet.component.css']
})
export class CarnetComponent implements OnInit {

  public listecarnet;
  public listecontact;

  constructor(private htpp: HttpClient, private carnetservice: CarnetserviceService) { }

  ngOnInit(): void {

    this.htpp.get("http://localhost:9091/carnets")
    .subscribe(data =>{
      this.listecarnet=data;
    }, error=>{
      console.log(error);
      
    });
  }

  onGetContact(carnet){
    this.htpp.get(carnet._links.contrats.href)
    .subscribe(data =>{
      this.listecontact=data;
      console.log(this.listecontact);
    }, error=>{
      console.log(error);
      
    });
  }


  onDelete(c){
    let conf = confirm("Etes Vous Sure !!!!");
    if(conf){
      console.log(c);
      this.carnetservice.deleteContrat(c._links.self.href)
      .subscribe(data =>{
      }, error=>{
        console.log(error);
        
      });
    }

  }

  onUpdate(c){

  }

}
