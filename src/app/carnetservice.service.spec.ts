import { TestBed } from '@angular/core/testing';

import { CarnetserviceService } from './carnetservice.service';

describe('CarnetserviceService', () => {
  let service: CarnetserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CarnetserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
